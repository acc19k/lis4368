

# LIS 4368 - Advanced Web Application Development

## Andrea Colon-De Feria

### Assignment 3 Requirements:

*The following will provide:*

1. Create an ERD based upon business rules
2. Provide screenshots of completed ERD and values
3. Provide DB resource links
4. Skillsets 4-6 with screenshots


#### MySQL ERD Screenshots:

*Screenshot of ERD model*
![Screenshot or ERD model with 3 tables](img/a3ERD_Screenshot.jpg)

*Screenshot of table 1 with ten values*
![Table 1 'pet' screenshot](img/a3PetScreenshot.jpg)

*Screenshot of table 2 with ten values*
![Table 2 'petstore' Screenshot](img/a3PetstoreScreenshot.jpg)

*Screenshot of table 3 with ten values*
![Table 3 'customer' Screenshot](img/a3CustomerScreenshot.jpg)

*Link to ERD:* [ERD file link](a3/../docs/a3_lis4368_model.mwb)
*Link to SQL file:* [sql file link](/a3/docs/a3lis4368b.sql)


#### Skillset Screenshots:

| *Skillset 4 screenshot* | *Skillset 5 screenshot*  | *Skillset 6 screenshot*  |
|-------------------------|--------------------------|--------------------------|
| ![ss4](../skillsets/ss4_Directory_Info/img/ss4_Screenshot.jpg) | ![skillset 5](../skillsets/ss5_Character_Info/img/ss5_screenshot.jpg)  | ![skillset 6](../skillsets/ss6_Determine_Character/img/ss6_screenshot.jpg)  |








