

# LIS4368 - Advanced Web Application Development

## Andrea Colon-De Feria

### Project 2 Requirements:

*Three parts:*

1. Use MVC to implement CRUD for basic server-side and client-side validation 
2. Implement update and delete functionalities
3. Chapter questions

#### README.md file should include the following items:

* Screenshot of basic server-side validation 
* Screenshot of thanks.jsp
* Screenshot of modified data and delete warning
  

#### Portfolio Screenshots:


|  *Screenshot of valid user-entry form* |  *Screenshot of passed validation* |
|----|----|
|  ![Valid user-entry form](img/p2Screenshot1.jpg)  |  ![passed validation](img/p2Screenshot2.jpg)  |



*Screenshot of displayed data*
![displayed data](img/p2Screenshot3.jpg)


*Screenshot of data being modified*
![data being modified](img/p2Screenshot4.jpg)

*Screenshot of modified data*
![modifyed data](img/p2Screenshot5.jpg)

*Screenshot of delete warning*
![delete warning](img/p2Screenshot6.jpg)

*Screenshot of deleted entry*
![deleted entry](img/p2Screenshot7.jpg)


|  *Screenshot of associated databse A*  |  *Screenshot of associated databse*  |
|----|----|
|  ![associated database](img/p2Screenshot8.jpg)  |  ![associated database](img/p2Screenshot9.jpg)  |







