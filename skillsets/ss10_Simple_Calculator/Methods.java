import java.util.Scanner;

class Methods
{

    //these first two will not have objects because they are are not for returning values
    public static void getRequirements(){
        System.out.println("Developer: Andrea Colon-De Feria");
        System.out.println("Program uses methods to:");
        System.out.println("add, subtract, multiply, divide, and exponentiations. Floating point numbers are rounded to two decimal places.");
        System.out.println("Note: Program checks for non-numeric values,and division by zero.");
    }

    public static void calculateNumbers(){

        double num1 = 0.0, num2 = 0.0;
        char operation = ' ';
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter mathematical opetaion (a=addition, s= subtraction,  m=multiplication, d=division, e=exponentiation): ");

        operation = sc.next().toLowerCase().charAt(0);

        while (operation !='a' && operation !='s' && operation !='m' && operation !='d' && operation !='e'){
            System.out.print("\nIncorrect operation. Please enter correct operation: ");
            operation = sc.next().toLowerCase().charAt(0);
        }

        
        System.out.println("Please enter first number: ");
        while(!sc.hasNextDouble()){
            System.out.println("Not valid number.");
            sc.next();
            System.out.print("Please try again, Enter first number: ");
        }

        num1 = sc.nextDouble();

        System.out.print("Please enter second number: ");
        while(!sc.hasNextDouble()){
            System.out.println("Not valid number.");
            sc.next();
            System.out.println("Please try again, Enter second number: ");
        }
        num2 = sc.nextDouble();

        //test operation
        if(operation == 'a'){ Addition(num1, num2);}
        else if(operation == 's'){Subtraction (num1, num2);}
        else if(operation == 'm'){Multiplication (num1, num2);}
        else if(operation == 's'){Subtraction (num1, num2);}
        else if(operation == 'd'){
            if(num2 == 0){
                System.out.println("Cannot divide by zero!");}
            else{
            Division (num1, num2);} } 

        else if(operation == 'e'){Exponentiation(num1, num2);}

                System.out.println();

                sc.close();

    }


        //these are for the mathematical methods!
    public static void Addition(double n1, double n2){
        System.out.print(n1 + " + " + n2 + " = ");
        System.out.printf("%.2f", (n1 + n2));
    }

    public static void Subtraction(double n1, double n2){
        System.out.print(n1 + " - " + n2 + " = ");
        System.out.printf("%.2f", (n1 - n2));
    }

    public static void Multiplication(double n1, double n2){
        System.out.print(n1 + " * " + n2 + " = ");
        System.out.printf("%.2f", (n1 * n2));
    }

    public static void Division(double n1, double n2){
        System.out.print(n1 + " / " + n2 + " = ");
        System.out.printf("%.2f", (n1 / n2));
    }

    public static void Exponentiation(double n1, double n2){
        System.out.print(n1 + " to the power of " + n2 + " = ");
        System.out.printf("%.2f", (Math.pow(n1,n2)));
    }

    

}



