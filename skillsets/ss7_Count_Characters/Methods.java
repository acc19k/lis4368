import java.util.Scanner;

public class Methods {
    public static void getRequirements(){

        System.out.println("Developer: Andrea Colon-De Feria");
        System.out.println("Program requirements:");
        System.out.println("\t1. Counts number and types of characters from the user-entered string.");
        System.out.println("\t2. Count: total, letters (upper-/loser-case), numbers, spaces, and other characters.");
        System.out.println("\t3. Hint: Helpful methods: isLetter(), isDigit(), isSpaceChar(), and others.");
        System.out.println();

    }

    public static void countChar(){

        //initialize variables
        int letter = 0;
        int space = 0;
        int num = 0;
        int upper = 0;
        int lower = 0;
        int other = 0;
        String str = "";

        //Here is a test string1!!
        System.out.print("Please enter string: ");
        Scanner sc = new Scanner(System.in);
        str = sc.nextLine();

        char[] ch = str.toCharArray();
        //we are entering nesting city. Think of these as AND operators at a larger scale

        for(int i=0; i < str.length(); i++){

            if(Character.isLetter(ch[i])){
                if(Character.isUpperCase(ch[i])){
                    upper++;
                }
                if(Character.isLowerCase(ch[i])){
                    lower++;
                }
                letter++;
            }
            else if(Character.isDigit(ch[i])){
                num++;
            }
            else if(Character.isSpaceChar(ch[i])){
                space++;
            }
            else{
                other++;
            }
        }

        System.out.println("\nYour string: \"" + str + "\" has the following number and types of characters: ");

        System.out.println("Total number of characters: " + str.length());
        System.out.println("Letter(s):" + letter);
        System.out.println("Upper-case letter(s): " + upper);
        System.out.println("Lower-case letter(s): " + lower);
        System.out.println("Number(s): " + num);
        System.out.println("Space(s): " + space);
        System.out.println("Other character(s): " + other);

        sc.close(); //it's good practice to close scanner 

    }

}//end of main 