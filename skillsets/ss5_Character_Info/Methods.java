import java.util.Scanner;

public class Methods {


public static void getRequirements()
{

System.out.println("Developer: Andrea Colon-De Feria");
System.out.println("Program determines total number of characters in line of text. \nas well as number of times specific cahracter is used.");
System.out.println("Program displays character\'s ASCII value.");

System.out.println();
}

public static void characterInfo()
{
//initialize variables
String str = "";
char ch = ' '; //unlike empty string *must* include character here--, space character!
int len = 0;
int num = 0;
Scanner sc = new Scanner(System.in);



System.out.print("Please enter a full line of text: ");
str = sc.nextLine();
len = str.length();

System.out.print("Please enter a character to check: ");

ch = sc.next().charAt(0);
for(int i = 0; i<len; i++)
{
if(ch == str.charAt(i))
{
++num;
}
}

System.out.println("\nNumber of characters in line of text: " + len);
System.out.println("The character " + ch + " apperars " + num + " time(s) in line of text.");
System.out.println("ASCII value: " + (int) ch);
sc.close();
}

}