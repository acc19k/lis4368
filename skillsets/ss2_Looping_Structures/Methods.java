import java.util.Scanner;

public class Methods{ 

    public static void getRequirements(){
        System.out.println("Developer: Andrea Colon-De Feria");
        System.out.println("Use following values: 1.0, 2.1, 3.2, 4.3, 5.4");
        System.out.println("Use followig loop structures: for, enhanced for, while, do...while.");
        System.out.println();
        System.out.println("Note: Pretest loops: for, enhanced for, while. Posttest loop: do...while.");
        System.out.println();


    }//end of getRequirements Method

    public static void arrayLoops(){

        //we create an array and populate it with all of the numbers
        float myFloats[] = {1.0f, 2.1f, 3.2f, 4.3f, 5.4f};

        System.out.println("for loop: ");
        for (int i = 0; i < myFloats.length; i++){

            System.out.println(myFloats[i]);

        }

        System.out.println("\nEnhanced for loop: ");
    

            for(float test : myFloats){
                System.out.println(test);
            }//end of enhanced for loop
    
            //now we create a  while loop where we declare variable before
            System.out.println("\nwhile loop: ");
            int i = 0;
            while(i < myFloats.length){
                System.out.println(myFloats[i]);
                i++;
            } //end of while loop 
            i = 0;
            //now we end with do... while loop aka post test loop!
            System.out.println("\ndo... while loop: ");
        
            do{
                System.out.println(myFloats[i]);
                i++;
            } while(i <myFloats.length); //end of do... while
    
    


                
            
        

    }//end of arrayLoops


}//end of main method