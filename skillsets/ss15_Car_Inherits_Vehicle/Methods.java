

public class Methods {
    public static void getRequirements(){

        System.out.println("Developer: Andrea Colon-De Feria");
        System.out.println("/////Below are base classndefault constructor values://///");
        System.out.println("Inside vehicle constructor.");
        System.out.println();
        System.out.println();
        System.out.println("Make= My Make");
        System.out.println("Model= My Model");
        System.out.println("Year= 1970");
        System.out.println();
        System.out.println();
        System.out.println("/////Below are base class user-entered values://///");
        System.out.println("Make: Ford");
        System.out.println("Model: Mustang GT500 KTR");
        System.out.println("Year: 2022");
        System.out.println();

        System.out.println("Inside vehicle constructor with parameters.");

        System.out.println();
        System.out.println("Make: Ford");
        System.out.println("Model: Mustang GT500 KTR");
        System.out.println("Year: 2022");
        System.out.println();
        System.out.println("////Below are derived class default constructor values:////");
        System.out.println("Make: Chevrolet, Model: Corvette Z06, Year: 2023");
        System.out.println();
        System.out.println("Make= My Make");
        System.out.println("Model= My Model");
        System.out.println("Year= 1970");
        System.out.println("Speed= 100.0");
        System.out.println();
        System.out.println("Or...");
        System.out.println("Make: My Make, Model: My Model, Year: 1970, Speed: 100.0");
        System.out.println();
        System.out.println("/////Below are derived class user-entered values://///");
        System.out.println("Make: Chevrolet");
        System.out.println("Model: COrvette");
        System.out.println("Year: 2023");
        System.out.println("Speed: 180");
        System.out.println();
        System.out.println("Inside vehicle constructor with parameters.");
        System.out.println("Inside car constructor with parameters");
        System.out.println();

        System.out.println("Make= Chevrolet");
        System.out.println("Model= Corvette");
        System.out.println("Year= 2023");
        System.out.println("Speed= 180.0");
        System.out.println();
        System.out.println("Or...");
        System.out.println("Make: Chevrolet, Model: Corvette, Year: 2023, Speed: 180.0");








    }
    }