

public class Methods {
    public static void getRequirements(){

        System.out.println("Developer: Andrea Colon-De Feria");
        System.out.println("/////Below are default constructor values://///");
        System.out.println("Inside vehicle constructor.");
        System.out.println();
        System.out.println();
        System.out.println("Make= My Make");
        System.out.println("Model= My Model");
        System.out.println("Year= 1970");
        System.out.println();
        System.out.println();
        System.out.println("/////Below are default constructor values://///");
        System.out.println();
        System.out.println();
        System.out.println("Make: Ford");
        System.out.println("Model: Mustang GT500 KTR");
        System.out.println("Year: 2022");
        System.out.println();

        System.out.println("Inside vehicle constructor.");

        System.out.println();
        System.out.println("Make: Ford");
        System.out.println("Model: Mustang GT500 KTR");
        System.out.println("Year: 2022");
        System.out.println();
        System.out.println("/////Below using setter methods to pass literal values, then print() method to display values://///");
        System.out.println("Make: Chevrolet, Model: Corvette Z06, Year: 2023");





    }
    }