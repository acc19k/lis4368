# LIS4368 - Advanced Web Applications Development

## Andrea Colon-De Feria

### Assignment 2 Requirements:

*Five parts:*

1. Create webapp and servlet query
2. Create ebooks database and include screenshots servlet
3. Include screenshots of functioning links
4. Java Skillsets 1-3
5. Chapter Questions (Chs 5-6)

#### README.md file should include the following items:

* Assessment links (as above), and  
* Screenshots (3 minimum): 1) querybook.html, 2) the query results, and 3) a screenshot of your a2/index.jsp file 
  

#### Assignment Screenshots:

*Screenshot of [localhost:9999/hello/index.html](http://localhost:9999/hello/index.html)*:

![Index file Hello World Screenshot](img/A2_Screenshot2.jpg)

*Screenshot of running servlet with random number [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello)*:

![Hello sevlet with random number generated](img/A2_Screenshot1.jpg)

*Screenshot of [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)*:

![Querybook file with two of the author options chosen](img/A2_Screenshot3.jpg)

*Screenshot of query results from chosen authors*

![Results from chosen authors](img/A2_Screenshot4.jpg)


#### Skillsets (click each to view it larger):

| *Skillset 1: System properties* | *Skillset 2: Looping structures*  | *Skillset 3: Number Swap*  |
|---------------------------------|-----------------------------------|----------------------------|
| [![Screenshot of running skillset 1](../skillsets/ss1_system_properties/img/ss1Screenshot.jpg)](../skillsets/ss1_system_properties/img/ss1Screenshot.jpg)  |  [![Screenshot of running skillset 2](../skillsets/ss2_Looping_Structures/img/ss2_Screenshot.jpg)](../skillsets/ss2_Looping_Structures/img/ss2_Screenshot.jpg)  | [![Screenshot of running skillset 3](../skillsets/ss3_Number_Swap/ss3Screenshot.jpg)](../skillsets/ss3_Number_Swap/ss3Screenshot.jpg)  |

