

# LIS4368 - Advanced Web Application Development

## Andrea Colon-De Feria

### Assignment 4 Requirements:

*Three parts:*

1. Use MVC to implement CRUD for basic server-side validation page
2. Java Skillsets
3. Chapter 11 and 12 questions

#### README.md file should include the following items:

* Screenshot of basic server-side validation 
* Screenshot of thanks.jsp
* Screenshot of java skillsets 10-12

#### Portfolio Screenshots:

*Screenshot of failed validation*
![Failed validaiton screen](img/a4Screenshot3.jpg)

*Screenshot of data in customerform*
![Screenshot of thank.jsp with data](img/a4Screenshot2.jpg)

*Screenshot of thanks.jsp*
![Screenshot of 11th entry in table](img/a4Screenshot1.jpg)

#### Skillset Screenshots:




