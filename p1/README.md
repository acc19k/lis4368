

# LIS4368 - Advanced Web Application Development

## Andrea Colon-De Feria

### Project 1 Requirements:

*Three parts:*

1. Modify .jsp file to include client-side data validation
2. Java Skillsets
3. Chapter 9 and 10 questions

#### README.md file should include the following items:

* Screenshot of failed client-side validation 
* Screenshot of passed client-side validation
* Screenshot of java skillsets 7-9

#### Portfolio Screenshots:

*Screenshot of failed validation*
![Screenshot of failed validation](img/p1Screenshot1.jpg)

*Screenshot of passed validation*
![Screenshot of passed validation on webapp](img/p1Screenshot2.jpg)

#### Skillset Screenshots:

*Skillset 7 screenshot* 
![ss7](../skillsets/ss7_Count_Characters/img/ss7Screenshot.jpg) 

*Skillset 8 screenshot*

![skillset 8](../skillsets/ss8_ASCII/img/ss7Gif.gif)

*Skillset 9 screenshot* 
![skillset 9](../skillsets/ss9_Grade_Calculator/img/ss9Screenshot.jpg) 

