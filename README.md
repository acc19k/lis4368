

# LIS4368 - Advanced Web Applications Development

## Andrea Colon-De Feria

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Git, Ampps, Tomcat, and JDK
    - Ensure installations are configured correctly
    - Clone BitBucket repos
    - Create web application
    - CH 1-4 questions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create webapp and servlet query
    - Create ebooks database and include screenshots servlet
    - Include screenshots of functioning links
    - Java Skillsets 1-3
    - Chapter Questions (Chs 5-6)
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create ERD based upon business rules
    - Provide screenshots of completed ERD
    - Provide DB resource links
    - Java skillsets 4-6
    - Chapter questions
4. [P1 README.md](p1/README.md "My Project 1 readme.md file")
    - Modified meta tags in jsp files
    - Added client-side validation with JavaScipt
    - Used regular expressions
    - Java skillsets 7-9
    - Chapter questions
5. [A4 ReadME.md](a4/README.md "Assignment 4 readme")  
    - Create customer forms 
    - Create customer servlets for users to enter data
    - Java skillsets 10-12
    - Chapter questions 
6. [A5 ReadME.md](a5/README.md "Assignment 5 readme file")
    - Connect to webapp to database 
    - Allow users to insert data into petstore
    -  Chapter questions
    -  Skillsets 13-15
7. [P2 readMe.md](p2/README.md "Project 2 readme link")
    - Add data table entries to webapp
    - Allow users to add data to customer table
    - Allow users to modify and delete data in customer table
    - Showcase connection to associated database