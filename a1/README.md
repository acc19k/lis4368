
# LIS4368 - Advanced Web Applications Development

## Andrea Colon-De Feria

### Assignment 1 Requirements:

*Three parts:*

1. Distribute version control with Git and BitBucket
2. Java/JSP/Server Development Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running Java Hello
* Screenshot of running localhost9999
* git commands with short descriptions
* Bitcucket repo links (class and bitbucketstationlocation)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Initiates git, and lets user access and edit a local repository. 
2. git status - Provides the status of all of the files created or changed, for instance, those that need to be commited or added.
3. git add - Add files or directories onto local repository.
4. git commit - Lets users commit changes made locally, prior to sending to remote repository.
5. git push - Allows users to push local entities into their remote repositories.
6. git pull - Allows users to pull, or accquire, the changes from a remote server to a local repository. 
7. git config - This command allows users to set repository options.

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK installation screenshots running "Hello World!"](img/javaScreenshot1.png)

*Successful Tomcat screenshot*:

![End of TomCat tutorial screen](img/tomcatScreenshot.png)

*Screenshot of running Web application*
![Screenshot of web application running A1 screenshots](img/runningWebapp.jpg)


#### Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial repository link](https://bitbucket.org/acc19k/bitbucketstationlocations/src/master/)

*Localhost LIS4368 link*
[link to webapp localhost](http://localhost:9999/lis4368/)


