

# LIS4368 - Advanced Web Application Development

## Andrea Colon-De Feria

### Assignment 5 Requirements:

*Three parts:*

1. Use MVC to implement CRUD and create forms for users to input data
2. Java Skillsets
3. Chapter 13 and 15 questions

#### README.md file should include the following items:

* Screenshot of basic server-side validation  to input in database
* Screenshot of thankyou.jsp page 
* Screenshot of MySql table with new entry
* Screenshot of java skillsets 13-15

#### Portfolio Screenshots:

*Screenshot of Assignment 5 customer form*
![Screenshot of customer form](img/a5Screenshot1.jpg)

*Screenshot of thanks.jsp*
![Screenshot of thank.jsp with data](img/a5Screenshot2.jpg)

*Screenshot of Customer table with Bensito Diaz*
![Screenshot of 11th entry in table](img/a5Screenshot3.jpg)

#### Skillset Screenshots:

*Skillset 13 screenshot* 
![ss13](../skillsets/ss13_File_Write_Read_Count_Words/img/ss13Screenshot.jpg) 

*Skillset 14 screenshot*

![skillset 14](../skillsets/ss14_Vehicle_Demo/img/ss14Screenshot.jpg)

*Skillset 15 screenshot* 

![skillset 15](../skillsets/ss15_Car_Inherits_Vehicle/img/ss15Screenshot.jpg) 
![skillset 15](../skillsets/ss15_Car_Inherits_Vehicle/img/ss15Screenshot2.jpg) 


